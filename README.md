# Thesis template

This template is forked from https://github.com/WojciechThomas/praca_inzynierska_latex but added configuration with VS Code

Docker image used: pastos/latex-pl:22.04
Repository to Dockerfile: https://gitlab.com/PastOSThesis/thesis-docker

## Usage

1. Make sure that **Dev containers** feature is available for you (please go through https://code.visualstudio.com/docs/devcontainers/containers#_installation)
2. Clone repository
3. Open repository in VS Code
4. Click **Ctl+Shift+P** (to open command pallet) and type **Dev Containers: Reopen in Container**
5. Recompile should work on saving .tex file or you can manually trigger it by **Ctrl+Alt+B**
